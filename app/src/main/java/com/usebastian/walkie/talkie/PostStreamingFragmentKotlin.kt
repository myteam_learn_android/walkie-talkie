package com.usebastian.walkie.talkie;

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

class PostStreamingFragmentKotlin : Fragment(), AudioStreamConnectionListener
{




    private val STREAMING_STATUS_CONNECTED = 1
    private val STREAMING_STATUS_DISCONNECTED = 0
    private val STREAMING_STATUS_ERROR = 0

    private var streamingStatus = STREAMING_STATUS_DISCONNECTED

    val REQUEST_RECORD_AUDIO = 111

    private var btnStartStopStreaming : AppCompatButton? = null

    var threadForTheStreaming: AudioRecordFromAndroidMicrophoneAndSendViaSocket? = null

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View?
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.post_streaming, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        btnStartStopStreaming = view?.findViewById(R.id.btnStartStopStreaming2)


        handleAskPermissionToRecordAudio()

        handleLiveStreamingButton()
    }

    fun handleAskPermissionToRecordAudio()
    {
        val permissionCheck = ContextCompat.checkSelfPermission(context!!, Manifest.permission.RECORD_AUDIO)

        if (permissionCheck != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.RECORD_AUDIO), REQUEST_RECORD_AUDIO)
        }
        else
        {
            //do nothing, permissions already added

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode)
        {
            REQUEST_RECORD_AUDIO -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {

                //do nothing
            }

            else ->
            {
                Toast.makeText(context, "Record audio permission is required to start audio streaming", Toast.LENGTH_LONG).show()
            }
        }
    }




    private fun handleLiveStreamingButton()
    {
        if (streamingStatus == STREAMING_STATUS_CONNECTED)
        {
            //tvStreamingStatus.text = getString(R.string.streaming_status_for_guide_on)
            btnStartStopStreaming?.text = "Stop"
        }
        else if (streamingStatus == STREAMING_STATUS_DISCONNECTED)
        {
            //tvStreamingStatus.text = getString(R.string.streaming_status_for_guide_off)
            btnStartStopStreaming?.text = "Start"
        }
        else
        {
            //error
            //tvStreamingStatus.text = getString(R.string.streaming_status_for_guide_error)
            btnStartStopStreaming?.text =" Start"
        }


        btnStartStopStreaming?.setOnClickListener(View.OnClickListener {


            val permissionCheck = ContextCompat.checkSelfPermission(context!!, Manifest.permission.RECORD_AUDIO)
            if (permissionCheck != PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(context, "Record audio permission is required to start audio streaming", Toast.LENGTH_LONG).show()
                return@OnClickListener
            }


            when (streamingStatus)
            {
                STREAMING_STATUS_DISCONNECTED ->
                {
                    connectAndStartStreaming()
                }

                STREAMING_STATUS_CONNECTED ->
                {
                    disconnectFromStreaming()
                }
                else -> //error
                {
                    connectAndStartStreaming()
                }
            }


        })
    }

    private fun connectAndStartStreaming()
    {

        if (threadForTheStreaming == null)
        {
            threadForTheStreaming = AudioRecordFromAndroidMicrophoneAndSendViaSocket()
            threadForTheStreaming?.connectionListener = this
            threadForTheStreaming?.pleaseProceed()
        }
        else
        {
            Toast.makeText(context, "Audio streaming already connected. You can speak now", Toast.LENGTH_LONG).show()
        }

    }


    private fun disconnectFromStreaming()
    {
        threadForTheStreaming?.close()
        threadForTheStreaming = null
    }


    override fun onConnected()
    {
        activity?.runOnUiThread(Runnable {
            streamingStatus = STREAMING_STATUS_CONNECTED
            handleLiveStreamingButton()
        })
    }

    override fun onError()
    {
        activity?.runOnUiThread(Runnable {
            streamingStatus = STREAMING_STATUS_ERROR
            handleLiveStreamingButton()
        })
    }

    override fun onDisconnected()
    {
        activity?.runOnUiThread(Runnable {
            streamingStatus = STREAMING_STATUS_DISCONNECTED
            handleLiveStreamingButton()
        })
    }

    override fun onDestroy()
    {
        threadForTheStreaming?.close()
        threadForTheStreaming = null

        super.onDestroy()
    }


    companion object
    {
        val BASE_URL_SERVER_AUDIO_STREAM = "192.168.1.10"

        fun getAudioStreamServerUrl(): String
        {

            return BASE_URL_SERVER_AUDIO_STREAM
        }
    }


//    fun handleRecordAudioFromAndroidMicrophoneAndSendToServer()
////    {
////        rec_mic_and_send_to_server.setOnClickListener(View.OnClickListener {
////
////            thread = AudioRecordFromAndroidMicrophoneAndSendViaSocket()
////        })
////
////        stop_rec_mic_and_send_to_server.setOnClickListener(View.OnClickListener {
////
////            thread?.close()
////        })
////    }

}