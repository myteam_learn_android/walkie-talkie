package com.usebastian.walkie.talkie;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class AudioRecordFromAndroidMicrophoneAndSendViaSocket
		extends Thread
{
	private boolean stopped = false;

	public AudioStreamConnectionListener connectionListener; //to be set if we want to observe the connection

	/**
	 * Give the thread high priority so that it's not canceled unexpectedly, and start it
	 */
	public AudioRecordFromAndroidMicrophoneAndSendViaSocket()
	{
		android.os.Process.setThreadPriority( android.os.Process.THREAD_PRIORITY_URGENT_AUDIO );
	}

	public void pleaseProceed()
	{
		start();
	}

	@Override
	public void run()
	{
		Log.i( "Audio", "Running Audio Thread" );
		AudioRecord recorder = null;
		//AudioTrack track = null;
		byte[][] buffers = new byte[256][160];
		int ix = 0;

		/*
		 * Initialize buffer to hold continuously recorded audio data, start recording, and start
		 * playback.
		 */
		try
		{
			int N = AudioRecord.getMinBufferSize( 8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT );

			//Utils.Companion.logInfo( "MMM = "+N ); 640

			recorder = new AudioRecord( MediaRecorder.AudioSource.MIC, 8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, N * 10 );//ar fi 6400 = 640*10
			//track = new AudioTrack( AudioManager.STREAM_MUSIC, 8000,AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, N*10, AudioTrack.MODE_STREAM);
			recorder.startRecording();
			//track.play();
			/*
			 * Loops until something outside of this thread stops it.
			 * Reads the data from the recorder and writes it to the audio track for playback.
			 */

			String hostname = PostStreamingFragmentKotlin.Companion.getAudioStreamServerUrl();
			int port = 17;
			InetAddress address = InetAddress.getByName( hostname );
			DatagramSocket socket = new DatagramSocket();

			//trimit un ping la server, sa il trezesc

			DatagramPacket pingRequest = new DatagramPacket( new byte[1], 1, address, port );
			socket.send( pingRequest );

			if( connectionListener != null )
			{
				connectionListener.onConnected();
			}

			while( !stopped )
			{
				//Log.i( "Map", "Writing new data to buffer" );
				byte[] buffer = buffers[ix++ % buffers.length];

				//Utils.Companion.logInfo( "PPP = "+buffer.length ); //160

				N = recorder.read( buffer, 0, buffer.length );

				//Utils.Companion.logInfo( "NNN = "+N ); 160

				//Utils.Companion.logInfo( "QQQ = "+buffer.length ); 160

				//track.write(buffer, 0, buffer.length);

				//---------de aici trimit pe socket bufferul inregistrat----------

				DatagramPacket request = new DatagramPacket( buffer, N, address, port );
				socket.send( request );

				//Thread.sleep(10000);
			}

			//------------pana aici trimit pe socket-------------------------

		}
		catch( Throwable x )
		{
			Log.w( "Audio", "Error reading voice audio", x );
			if( connectionListener != null )
			{
				connectionListener.onError();
			}
		}
		/*
		 * Frees the thread's resources after the loop completes so that it can be run again
		 */
		finally
		{
			recorder.stop();
			recorder.release();
//			track.stop();
//			track.release();
		}
	}

	/**
	 * Called from outside of the thread in order to stop the recording/playback loop
	 */
	void close()
	{
		stopped = true;
		if( connectionListener != null )
		{
			connectionListener.onDisconnected();
		}
	}
}