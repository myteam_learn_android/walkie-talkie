package com.usebastian.walkie.talkie;

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment


class GetStreamingFragmentKotlin : Fragment(), AudioStreamConnectionListener
{


    private val STREAMING_STATUS_CONNECTED = 1
    private val STREAMING_STATUS_DISCONNECTED = 0
    private val STREAMING_STATUS_ERROR = 0

    private var streamingStatus = STREAMING_STATUS_DISCONNECTED

    private var btnStartStopStreaming :AppCompatButton? = null

    var threadForTheStreaming: AudioPlayFromAndroidWhatIsReceivedViaSocket? = null


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View?
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.get_streaming, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)
        btnStartStopStreaming = view?.findViewById(R.id.btnStartStopStreaming)

        handleLiveStreamingButton()
    }



    private fun handleLiveStreamingButton()
    {
        if (streamingStatus == STREAMING_STATUS_CONNECTED)
        {
            //tvStreamingStatus.text = getString(R.string.streaming_status_for_tourist_on)
            btnStartStopStreaming?.text = "Stop"
        }
        else if (streamingStatus == STREAMING_STATUS_DISCONNECTED)
        {
            //tvStreamingStatus.text = getString(R.string.streaming_status_for_tourist_off)
            btnStartStopStreaming?.text = "Start"
        }
        else
        {
            //error
            //tvStreamingStatus.text = getString(R.string.streaming_status_for_guide_error)
            btnStartStopStreaming?.text = "Start"
        }


        btnStartStopStreaming?.setOnClickListener(View.OnClickListener {


            val permissionCheck = ContextCompat.checkSelfPermission(context!!, Manifest.permission.RECORD_AUDIO)
            if (permissionCheck != PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(context, "Record audio permission is required to start audio streaming", Toast.LENGTH_LONG).show()
                return@OnClickListener
            }


            when (streamingStatus)
            {
                STREAMING_STATUS_DISCONNECTED ->
                {
                    connectAndStartStreaming()
                }

                STREAMING_STATUS_CONNECTED ->
                {
                    disconnectFromStreaming()
                }
                else -> //error
                {
                    connectAndStartStreaming()
                }
            }


        })
    }

    private fun connectAndStartStreaming()
    {

        if (threadForTheStreaming == null)
        {
            threadForTheStreaming = AudioPlayFromAndroidWhatIsReceivedViaSocket()
            threadForTheStreaming?.ServerUrlToConnect = getAudioStreamServerUrl()
            threadForTheStreaming?.connectionListener = this
            threadForTheStreaming?.pleaseProceed()
        }
        else
        {
           Toast.makeText(context, "Audio streaming already connected. Please adjust your phone volume if you can't hear anything", Toast.LENGTH_LONG).show()
        }

    }


    private fun disconnectFromStreaming()
    {
        threadForTheStreaming?.close()
        threadForTheStreaming = null
    }


    override fun onConnected()
    {
        activity?.runOnUiThread(Runnable {
            streamingStatus = STREAMING_STATUS_CONNECTED
            handleLiveStreamingButton()
        })
    }

    override fun onError()
    {
        activity?.runOnUiThread(Runnable {
            streamingStatus = STREAMING_STATUS_ERROR
            handleLiveStreamingButton()
        })
    }

    override fun onDisconnected()
    {
        activity?.runOnUiThread(Runnable {
            streamingStatus = STREAMING_STATUS_DISCONNECTED
            handleLiveStreamingButton()
        })
    }

    override fun onDestroy()
    {
        threadForTheStreaming?.close()
        threadForTheStreaming = null

        super.onDestroy()
    }

    fun getAudioStreamServerUrl(): String
    {
        val guideId = arguments?.getString("guideId")
        if (guideId != null)
        {
            return PostStreamingFragmentKotlin.BASE_URL_SERVER_AUDIO_STREAM
        }
        else
        {
            return PostStreamingFragmentKotlin.BASE_URL_SERVER_AUDIO_STREAM
        }
    }


}