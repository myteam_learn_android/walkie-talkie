package com.usebastian.walkie.talkie;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.util.Log;
import android.widget.Toast;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.time.Duration;

public class AudioPlayFromAndroidWhatIsReceivedViaSocket extends Thread
{

	private boolean stopped = false;

	public AudioStreamConnectionListener connectionListener; //to be set if we want to observe the connection

	public String ServerUrlToConnect;

	public AudioPlayFromAndroidWhatIsReceivedViaSocket()
	{
		android.os.Process.setThreadPriority( android.os.Process.THREAD_PRIORITY_URGENT_AUDIO );
	}

	public void pleaseProceed()
	{
		start();
	}

	@Override
	public void run()
	{

		AudioTrack track = null;
		try
		{

			int N = AudioRecord.getMinBufferSize( 8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT );
			track = new AudioTrack( AudioManager.STREAM_MUSIC, 8000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, N * 10, AudioTrack.MODE_STREAM );
			track.play();

			String hostname = ServerUrlToConnect;
			int port = 12555;//18 e port de get
			InetAddress address = InetAddress.getByName( hostname );
			DatagramSocket socket = new DatagramSocket();

			//trimit un ping la server, sa il trezesc

			DatagramPacket pingRequest = new DatagramPacket( new byte[1], 1, address, port );
			socket.send( pingRequest );

			if( connectionListener != null )
			{
				connectionListener.onConnected();
			}

			while( !stopped )
			{
				byte[] buffer = new byte[6400];
				DatagramPacket response = new DatagramPacket( buffer, buffer.length );
				socket.receive( response );

				track.write( buffer, 0, response.getLength() );

//				String quote = new String(buffer, 0, response.getLength());
//				Utils.Companion.logInfo( "Am prirmit: "+quote );
			}
		}
		catch( Throwable e )
		{
			Log.w("123",  "Ceva a crapat la play" );
			if( connectionListener != null )
			{
				connectionListener.onError();
			}
		}

		finally
		{

			track.stop();
			track.release();
		}
	}

	void close()
	{
		stopped = true;
		if( connectionListener != null )
		{
			connectionListener.onDisconnected();
		}
	}
}
